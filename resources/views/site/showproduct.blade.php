<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href="https://cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css" rel="stylesheet" />

</head>
@if(session()->has('success'))
    <div class="alert alert-success success_msg_div" >
        {{ session()->get('success') }}
    </div>
    @endif
<body>
<?php
$user_id = Auth::id();
?>
<div class="w3-container">
  <h2>Products</h2>
  <p>You can add product to the cart use below table:</p>
  
  <button style="margin-left: 1350px;"><i class="mdi mdi-cart"><a href="{{route('site.showcart')}}">Cart</a></i></button>
  <button style="margin-left: 1400px;"><a href="{{route('site.transaction')}}">Total Transactions</a></button>
  <button style="margin-left: 1430px;"><a href="{{route('site.login')}}">Logout</a></button>
  <table class="w3-table-all w3-small">
 
    <tr>
      <th>Image</th>
      <th>Name</th>
      <th>Price</th>
      <th>Quantity</th>
      <th>Add To Cart</th>
    </tr>

  @foreach ($products as $key=>$product)

  <tr>
      <td>
      @if($product->image!='') <img src="{{ asset('images/'.$product->image) }}" height="60px" width="50px">
       @else  
       <img src="{{ asset('admin/img/no-img.jpg') }}" height="60px" width="50px">
      @endif 
      </td>
      <td>{{$product->name}}</td>
      <td>{{$product->price}}</td>
      <td><input type="number" class="quantity_{{$product->id}}" name="quantity" /></td>
      <td><input type="button" user_id="{{$user_id}}" product_image="{{$product->image}}" product_name="{{$product->name}}" product_price="{{$product->price}}" product_id="{{$product->id}}" class="cart_button" value="Add To Cart" /></td>
  </tr>

  

  @endforeach
  </table>
</div>

</body>

<script>

$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
$(document).ready(function(){
  $(".cart_button").click(function(){
    
    var user_id = $(this).attr('user_id');
    var product_id = $(this).attr('product_id');
    var product_price = $(this).attr('product_price');
    var product_name = $(this).attr('product_name');
    var product_image = $(this).attr('product_image');
    var item_quantity = $('.quantity_'+product_id).val();
   if(item_quantity == ''){
     alert('please enter quantity');exit;
   }
    $.ajax({
           type:'POST',
           url:"{{ route('site.storecart') }}",
           data:{"_token": "{{ csrf_token() }}",user_id:user_id, product_id:product_id, product_price:product_price,product_name:product_name,item_quantity:item_quantity,product_image:product_image},
           success:function(data){
              alert(data.success);
           }
        });
       
        $('.quantity_'+product_id).val('');
    
  });
});
</script>

</html>
