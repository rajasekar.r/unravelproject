<!DOCTYPE html> 
  <html lang="en"> 
    <head>  
      <meta charset="UTF-8">  
      <meta name="viewport" content="width=device-width, initial-scale=1.0">                
      <title>User Registration Form</title>  
      <link rel="stylesheet" href="style.css">  
      <meta name="viewport" content="width=device-width, initial-scale=1"> 
 <style>
body {  
  background: #C5E1A5; 
}
 form {  
   width: 30%;  
   margin: 60px auto;  
   background: #efefef;  
   padding: 60px 120px 80px 120px;  
   text-align: center;  
   -webkit-box-shadow: 2px 2px 3px rgba(0,0,0,0.1); 
   box-shadow: 2px 2px 3px rgba(0,0,0,0.1); 
}

label {  
  display: block;  
  position: relative;  
  margin: 40px 0px; 
}
 .label-txt {  
   position: absolute;  
   top: -1.6em;  
   padding: 10px;  
   font-family: sans-serif;  
   font-size: .8em;  
   letter-spacing: 1px;  
   color: rgb(120,120,120);  
   transition: ease .3s; 
}
 .input {  
   width: 100%;  
   padding: 10px;  
   background: transparent;  
   border: none;  
   outline: none; 
}
.line-box {  
  position: relative;  
  width: 100%;  
  height: 2px;  
  background: #BCBCBC; 
}
 .line {
   position: absolute;  
   width: 0%;  
   height: 2px;  
   top: 0px;  
   left: 50%;  
   transform: translateX(-50%);  
   background: #8BC34A;  
   transition: ease .6s; 
}  
 .input:focus + .line-box .line {  
   width: 100%; 
}
 .label-active {  
   top: -3em; 
}
 button {  
   display: inline-block; 
   padding: 12px 24px;  
   background: rgb(220,220,220);  
   font-weight: bold;  
   color: rgb(120,120,120);  
   border: none;  outline: none;  
   border-radius: 3px;  
   cursor: pointer;  
   transition: ease .3s; 
}  
 button:hover {  
   background: #8BC34A;  
   color: #ffffff; 
}
</style>
    </head>
    <body> 
        <div>   
          <form method="post" action="{{ route('site.storeuser') }}" enctype="multipart/form-data">   
          {{ csrf_field() }} 
            <h4 class="text-warning text-center pt-5">User Registration</h4>  
            <label>     
              <input type="text" class="input" name="name" placeholder="Name"required />                   
              <div class="line-box">          
              <div class="line"></div>        
              </div>    
            </label>
            <label>     
              <input type="text" class="input" name="email" placeholder="EMAIL" required />                   
              <div class="line-box">          
              <div class="line"></div>        
              </div>    
            </label>   
            <label>
              <input type="password" class="input" name="password" placeholder="PASSWORD" required />        
              <div class="line-box">          
                <div class="line"></div>        
              </div>    
            </label>
            
            <label>     
              <input type="tel" class="input" name="mobile" placeholder="MOBILE" required />        
              <div class="line-box">         
                <div class="line"></div>        
              </div>    
            </label> 

            <label>     
            <select class="input" name="gender" id="gender">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="Transgender">Transgender</option>
            </select>        
              <div class="line-box">         
                <div class="line"></div>        
              </div>    
            </label> 

            <label>     
              <input type="file" class="input" name="image" placeholder="Image" required />        
              <div class="line-box">         
                <div class="line"></div>        
              </div>    
            </label> 

            <button type="submit">submit</button> 
            <button type="submit"><a href="{{ route('site.login') }}">Already have Account? Click here to login</a></button>  
          </form> 
      </div> 
    </body> 
</html>