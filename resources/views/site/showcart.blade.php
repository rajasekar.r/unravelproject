<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href="https://cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css" rel="stylesheet" />

</head>
<body>
<?php
$user_id = Auth::id();
?>
<div class="w3-container">
  <h2>Cart</h2>
  <p>Your Cart Page:</p>
  <button style="margin-left: 1400px;"><a href="{{route('site.transaction')}}">Total Transactions</a></button>
  <button style="margin-left: 1430px;"><a href="{{route('site.login')}}">Logout</a></button>
  
  
  <table class="w3-table-all w3-small">
 
    <tr>
      <th>Image</th>
      <th>Name</th>
      <th>Quantity</th>
      <th>Price</th>
      <th>Total Price</th>
    </tr>
    <?php $total = 0; ?>
  @foreach ($products as $key=>$product)

  <tr>
  <?php  $total = $total+$product->total_price ?>
      <td>
      @if($product->product_image!='') <img src="{{ asset('images/'.$product->product_image) }}" height="60px" width="50px">
       @else  
       <img src="{{ asset('admin/img/no-img.jpg') }}" height="60px" width="50px">
      @endif 
      </td>
      <td>{{$product->product_name}}</td>
      <td>{{$product->quantity}}</td>
      <td>{{$product->price}}</td>
      <td>{{$product->total_price}}</td>
      </tr>
      

  @endforeach

  <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td><h4>Total : {{ $total }} </h4>
      <button style="color:Green;width:100px;height:35px"><a href="{{route('site.checkout')}}">Checkout</a></button>
      </td>
      </tr>

  </td>
  
 

  </table>
</div>

</body>


</html>
