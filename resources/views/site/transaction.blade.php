<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<head>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link href="https://cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css" rel="stylesheet" />

</head>
<body>
<div class="w3-container">
  <h2>Your Transaction</h2>
  <button style="margin-left: 1350px;"><i class="mdi mdi-cart"><a href="{{route('site.showcart')}}">Cart</a></i></button>
  <button style="margin-left: 1430px;"><a href="{{route('site.login')}}">Logout</a></button>
  
  
  <table class="w3-table-all w3-small">
 
    <tr>
      <th>Full Name</th>
      <th>Email</th>
      <th>Address</th>
      <th>City</th>
      <th>State</th>
      <th>Zip</th>
      <th>Total Item</th>
      <th>Total Amount</th>
      <th>Payment Date</th>
    </tr>
    <?php $total = 0; ?>
  @foreach ($products as $key=>$product)

  <tr>
      <td>{{$product->fullname}}</td>
      <td>{{$product->email}}</td>
      <td>{{$product->address}}</td>
      <td>{{$product->city}}</td>
      <td>{{$product->state}}</td>
      <td>{{$product->zip}}</td>
      <td>{{$product->total_item}}</td>
      <td>{{$product->total_amount}}</td>
      <td>{{$product->created_at}}</td>
      </tr>
      

  @endforeach

  </td>
  
 

  </table>
</div>

</body>


</html>
