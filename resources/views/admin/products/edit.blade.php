@extends('admin.layouts.app')

@section('content')

<div class="content-wrapper">
        <div class="content">	
        <div class="row">
								<div class="col-lg-9">
									<div class="card card-default">
										<div class="card-header card-header-border-bottom">
											<h2>Edit Product</h2>
										</div>
										<div class="card-body">
                                        <form  method="post" action="{{ route('admins.updateproduct') }}" enctype="multipart/form-data">
  			                                {{ csrf_field() }}
												<div class="form-group">
													<label for="exampleFormControlInput1">Name</label>
													<input type="text" class="form-control" id="name" name="name"  value="{{ $product[0]->name }}" placeholder="Enter Product Name" required>
													<input type="hidden" class="form-control" id="hidd_id" name="hidd_id"  value="{{ $product[0]->id }}" >
													<input type="hidden" class="form-control" id="hidd_image" name="hidd_image"  value="{{ $product[0]->image }}" >
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Price</label>
													<input type="number" class="form-control" id="price" name="price"value="{{ $product[0]->price }}"  placeholder="Enter Product Price" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlFile1">Image</label>
													<input type="file" class="form-control-file" id="image" name="image">
												</div>
												@if($product[0]->image!='') <img src="{{ asset('images/'.$product[0]->image) }}" height="60px" width="50px">
                                                    @else  
                                                    <img src="{{ asset('admin/img/no-img.jpg') }}" height="60px" width="50px">
                                                @endif 
                                                
												<div class="form-footer pt-4 pt-5 mt-4 border-top">
													<button type="submit" class="btn btn-primary btn-default">Submit</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							
							</div>
@endsection