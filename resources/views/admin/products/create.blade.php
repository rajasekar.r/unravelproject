@extends('admin.layouts.app')

@section('content')

<div class="content-wrapper">
        <div class="content">	
        <div class="row">
								<div class="col-lg-9">
									<div class="card card-default">
										<div class="card-header card-header-border-bottom">
											<h2>Create Product</h2>
										</div>
										<div class="card-body">
                                        <form  method="post" action="{{ route('admins.storeproduct') }}" enctype="multipart/form-data">
  			                                {{ csrf_field() }}
												<div class="form-group">
													<label for="exampleFormControlInput1">Name</label>
													<input type="text" class="form-control" id="name" name="name" placeholder="Enter Product Name" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlInput1">Price</label>
													<input type="number" class="form-control" id="price" name="price" placeholder="Enter Product Price" required>
												</div>

												<div class="form-group">
													<label for="exampleFormControlFile1">Image</label>
													<input type="file" class="form-control-file" id="image" name="image" required>
												</div>
                                                
												<div class="form-footer pt-4 pt-5 mt-4 border-top">
													<button type="submit" class="btn btn-primary btn-default">Submit</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							
							</div>
@endsection