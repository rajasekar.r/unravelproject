
      <footer class="footer mt-auto">
            <div class="copyright bg-white">
              <p>
                &copy; <span id="copy-year"></span> Copyright by Unravel 
              
              </p>
            </div>
            <script>
                var d = new Date();
                var year = d.getFullYear();
                document.getElementById("copy-year").innerHTML = year;
            </script>
        </footer>
    </div>
  </div>
  
<script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('admin/plugins/slimscrollbar/jquery.slimscroll.min.js') }}"></script>


<script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
<script src="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill.js') }}"></script>

<script src="{{ asset('admin/plugins/toastr/toastr.min.js') }}"></script>



<script src="{{ asset('admin/js/sleek.bundle.js') }}"></script>
<script>
    setTimeout(function(){ $('.success_msg_div').hide(); }, 3000);
</script>

@stack('scripts')

</body>

</html>
