@extends('admin.layouts.app')

@section('content')
<div class="content-wrapper">
        <div class="content">	
            <div class="row">
                <div class="col-12">
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none recent-orders" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2></h2>
                    </div>
                    <div class="card-body pt-0 pb-5">
                      <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                        <thead>
                          <tr class="table-striped">
                            <th class="d-none d-lg-table-cell">Name</th>
                            <th class="d-none d-lg-table-cell">Email</th>
                            <th class="d-none d-lg-table-cell">Address</th>
                            <th class="d-none d-lg-table-cell">City</th>
                            <th class="d-none d-lg-table-cell">State</th>
                            <th class="d-none d-lg-table-cell">Zip</th>
                            <th class="d-none d-lg-table-cell">Total Item</th>
                            <th class="d-none d-lg-table-cell">Total Amount</th>
                            <th class="d-none d-lg-table-cell">Created At</th>

                           
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $key=>$product)
                          <tr>
                            <td class="d-none d-lg-table-cell">{{ $product->fullname  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $product->email  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $product->address  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $product->city  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $product->state  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $product->zip  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $product->total_item  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $product->total_amount  }}</td>

                            <td class="d-none d-lg-table-cell">{{ $product->created_at->diffForHumans()  }}</td>

                            <td class="text-right">
                              <div class="dropdown show d-inline-block widget-dropdown">
                                <a class="dropdown-toggle icon-burger-mini" href="" role="button" id="dropdown-recent-order1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-recent-order1">
                                  <li class="dropdown-item">
                                    <a href="{{ url('/admins/transactionview') }}/{{ $product->id }}">View</a>
                                  </li>
                                 
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
</div>
							</div>
@push('scripts')
@endpush
@endsection