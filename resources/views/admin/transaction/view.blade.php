@extends('admin.layouts.app')

@section('content')
<div class="content-wrapper">
        <div class="content">	
            <div class="row">
                <div class="col-12">
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none recent-orders" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2> Transaction View</h2>
                    </div>
                    <div class="card-body pt-0 pb-5">
                      Name : {{ $transaction[0]->fullname }} </br>
                      Email : {{ $transaction[0]->email }} </br>
                      Address : {{ $transaction[0]->address }} </br>
                      City : {{ $transaction[0]->city }} </br>
                      State : {{ $transaction[0]->state }} </br>
                      Zip : {{ $transaction[0]->zip }} </br>
                      Total Item : {{ $transaction[0]->total_item }} </br>
                      Total Amount : {{ $transaction[0]->total_amount }} </br>
                      Purchased At : {{ $transaction[0]->created_at }} </br>

                    </div>
                  </div>
</div>
							</div>
@push('scripts')

@endpush
@endsection