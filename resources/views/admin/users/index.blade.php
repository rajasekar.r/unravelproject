@extends('admin.layouts.app')

@section('content')
<div class="content-wrapper">
        <div class="content">	
            <div class="row">
                <div class="col-12">
                  <!-- Recent Order Table -->
                  <div class="card card-table-border-none recent-orders" id="recent-orders">
                    <div class="card-header justify-content-between">
                      <h2></h2>
                    </div>
                    <div class="card-body pt-0 pb-5">
                      <table class="table card-table table-responsive table-responsive-large" style="width:100%">
                        <thead>
                          <tr class="table-striped">
                            <th class="d-none d-lg-table-cell">Name</th>
                            <th class="d-none d-lg-table-cell">Email</th>
                            <th class="d-none d-lg-table-cell">Mobile</th>
                            <th class="d-none d-lg-table-cell">Gender</th>
                            <th class="d-none d-lg-table-cell">Status</th>
                            <th class="d-none d-lg-table-cell">Image</th>
                            <th class="d-none d-lg-table-cell">Crteated At</th>
                           
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($clients as $key=>$client)
                          <tr>
                            <td class="d-none d-lg-table-cell">{{ $client->name  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $client->email  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $client->mobile  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $client->gender  }}</td>
                            <td class="d-none d-lg-table-cell">{{ $client->status  }}</td>
                            <td class="d-none d-lg-table-cell">@if($client->image!='') <img src="{{ asset('images/'.$client->image) }}" height="60px" width="50px">
                                                    @else  
                                                    <img src="{{ asset('admin/img/no-img.jpg') }}" height="60px" width="50px">
                                                @endif 
                            </td>

                            <td class="d-none d-lg-table-cell">{{ $client->created_at->diffForHumans()  }}</td>

                            <td class="text-right">
                              <div class="dropdown show d-inline-block widget-dropdown">
                                <a class="dropdown-toggle icon-burger-mini" href="" role="button" id="dropdown-recent-order1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static"></a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-recent-order1">
                                  <li class="dropdown-item">
                                    <a href="{{ url('/admins/userview') }}/{{ $client->id }}">View</a>
                                  </li>
                                </ul>
                              </div>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                    </div>
                  </div>
</div>
							</div>
@push('scripts')
@endpush
@endsection