<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="description" content="Sleek Dashboard - Free Bootstrap 4 Admin Dashboard Template and UI Kit. It is very powerful bootstrap admin dashboard, which allows you to build products like admin panels, content management systems and CRMs etc.">


  <title>Unravel Project</title>

  <!-- GOOGLE FONTS -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500" rel="stylesheet" />
  <link href="https://cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css" rel="stylesheet" />


  <!-- PLUGINS CSS STYLE -->
  <link href="{{ asset('admin/plugins/nprogress/nprogress.css') }}" rel="stylesheet" />
  
  <link href="{{ asset('admin/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" />
  
  <link href="{{ asset('admin/plugins/toastr/toastr.min.css') }}" rel="stylesheet" />

  <!-- SLEEK CSS -->
  <link id="sleek-css" rel="stylesheet" href="{{ asset('admin/css/sleek.css') }}" />

  <!-- FAVICON -->
  <link href="{{ asset('admin/img/favicon.png') }}" rel="shortcut icon" />

<div class="container d-flex align-items-center justify-content-center vh-100">
      <div class="row justify-content-center">
        <div class="col-lg-7 col-md-10">
          <div class="card">
            <div class="card-header bg-primary">
              <div class="app-brand">
              <img src="{{ asset('admin/img/admin-logo.png') }}" alt="Admin Logo" width="385" height="100">
              </div>
            </div>
            <div class="card-body p-5">

              <h4 class="text-dark mb-5">Sign In</h4>
              <form method="POST" action="{{ route('admins.handleLogin') }}" class="m-login__form m-form">
                                     @csrf
                <div class="row">
                  <div class="form-group col-md-12 mb-4">
                    <input type="email" class="form-control input-lg" id="email" name="email" aria-describedby="emailHelp" placeholder="Username">
                  </div>
                  <div class="form-group col-md-12 ">
                    <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password">
                  </div>
                  <div class="col-md-12">
                    <div class="d-flex my-2 justify-content-between">
                      <div class="d-inline-block mr-3">
                        <label class="control control-checkbox">Remember me
                          <input type="checkbox" />
                          <div class="control-indicator"></div>
                        </label>

                      </div>
                      <!--<p><a class="text-blue" href="#">Forgot Your Password?</a></p>-->
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary btn-block mb-4">Sign In</button>
                    <!--<p>Don't have an account yet ?
                      <a class="text-blue" href="sign-up.html">Sign Up</a>
                    </p>-->
                  </div>
                </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>
 <!-- jQuery v3.4.1 -->
 <script src="lib/jquery/jquery-3.4.1.min.js"></script>
  <!--  Bootstrap v4.3.1 JS -->
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <!-- Magnific Popup core JS file -->
  <script src="lib/Magnific-Popup-master/dist/jquery.magnific-popup.js"></script>
  <!-- Slick JS -->
  <script src="lib/slick/slick/slick.min.js"></script>
  <!--  Custom JS -->
  <script src="js/theme.js"></script>