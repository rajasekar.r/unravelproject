<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Product;

class UserController extends Controller
{
    public function index(){
        $sclients = User::all();
        return view('admin.users.index')->with('clients', $sclients); 
    }

    public function view($id){
        $sclients = User::where('id',$id)->get();
        return view('admin.users.view')->with('clients', $sclients); 
    }

    public function login(){
        return view('site.login');
    }

    public function logout(){
        Auth::logout();
        return view('site.login');
    }

    public function handleLogin(Request $request){
        $credentials = $request->only('email', 'password');
         if(Auth::guard('web')->attempt($credentials)){
            //$sproduct = Product::all();
            //return view('site.showproduct')->with('products', $sproduct);
            return redirect()->route('site.showproduct');
         }else{
             echo 'failure';
         }
    }

    public function registration(){
        return view('site.register');
    }

    public function storeuser(Request $request){
        $name = $request->input('name');
        $email = $request->input('email');
        $input_pass = $request->input('password');
        $password = Hash::make($input_pass);
        $mobile = $request->input('mobile');
        $gender = $request->input('gender');
        $imageName = '';
        $request->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);

        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);

        $registerdetails = new User();
        $registerdetails->name = $name;
        $registerdetails->email = $email;
        $registerdetails->password = $password;
        $registerdetails->mobile = $mobile;
        $registerdetails->gender = $gender;
        $registerdetails->image = $imageName;
        $registerdetails->save();
       
        return redirect()->route('site.login')->with('success','Registered Successfully');
    }
}
