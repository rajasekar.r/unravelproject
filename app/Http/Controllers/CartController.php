<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Transaction;

class CartController extends Controller
{
    public function showproduct(){
        $sproduct = Product::all();
        return view('site.showproduct')->with('products', $sproduct);
    }

    public function storecart(Request $request){
        
        $user_id = $request->input('user_id');
        $product_id = $request->input('product_id');
        $product_price = $request->input('product_price');
        $product_name = $request->input('product_name');
        $item_quantity = $request->input('item_quantity');
        $product_image = $request->input('product_image');
        


        $cartdetails = new Cart();
        $cartdetails->user_id = $user_id;
        $cartdetails->product_name = $product_name;
        $cartdetails->product_id = $product_id;
        $cartdetails->quantity = $item_quantity;
        $cartdetails->price = $product_price;
        $cartdetails->product_image = $product_image;
        $cartdetails->total_price = $product_price*$item_quantity;
        
        $cartdetails->save();

        return response()->json(['success'=>'Added in Cart.']);

    }

    public function showcart(){
        $user_id = Auth::id();
        $sproduct = Cart::where('user_id',$user_id)->get();
        return view('site.showcart')->with('products', $sproduct);
    }

    public function checkout(){
        $user_id = Auth::id();
        $sproduct = Cart::where('user_id',$user_id)->get();
        return view('site.checkout')->with('products', $sproduct);
    }

    public function storetransaction(Request $request){
        $name = $request->input('firstname');
        $email = $request->input('email');
        $address = $request->input('address');
        $city = $request->input('city');
        $state = $request->input('state');
        $zip = $request->input('zip');
        $user_id = $request->input('user_id');
        $total_item = $request->input('total_item');
        $total_amount = $request->input('total_amount');

        $transdetails = new Transaction();
        $transdetails->fullname = $name;
        $transdetails->email = $email;
        $transdetails->address = $address;
        $transdetails->city = $city;
        $transdetails->state = $state;
        $transdetails->zip = $zip;
        $transdetails->user_id = $user_id;
        $transdetails->total_item = $total_item;
        $transdetails->total_amount = $total_amount;
        
        $transdetails->save();

        $cart = Cart::where('user_id',$user_id);
        $cart->delete();
        return redirect()->route('site.showproduct')->with('success','Payment Completed and Order Placed');

    }

    public function transaction(){
        $user_id = Auth::id();
        $transactions = Transaction::where('user_id',$user_id)->get();
        return view('site.transaction')->with('products', $transactions);
    }

}
