<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use App\Models\Transaction;

class ProductController extends Controller
{
    public function index(){
        $sproduct = Product::all();
        return view('admin.products.index')->with('products', $sproduct); 
    }

    public function create(){
        return view('admin.products.create');
    }

    public function store(Request $request){
        $name = $request->input('name');
        $price = $request->input('price');
        
        $request->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);

        $imageName = time().'.'.$request->image->extension();  
   
        $request->image->move(public_path('images'), $imageName);

        $productdetails = new Product();
        $productdetails->name = $name;
        $productdetails->price = $price;
        $productdetails->image = $imageName;
        $productdetails->save();
       
        return redirect()->route('admins.productlist');

    }

    public function view($id){
        $sproducts = Product::where('id', $id)->get();
        return view('admin.products.view')->with('product',$sproducts);
    }

    public function edit($id){
        $sproducts = Product::where('id', $id)->get();
        return view('admin.products.edit')->with('product',$sproducts);
    }

    public function update(Request $request){

        $id = $request->input('hidd_id');

        $productdetails = Product::where('id', $id)->firstOrFail();

        $name = $request->input('name');
        $price = $request->input('price');
        $imageName = $request->input('hidd_image');
        if ($request->file('image')!='') {
            $request->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);

            $imageName = time().'.'.$request->image->extension();  
   
            $request->image->move(public_path('images'), $imageName);
        }
        $productdetails->name = $name;
        $productdetails->price = $price;
        $productdetails->image = $imageName;
        $productdetails->save();
       
        return redirect()->route('admins.productlist')->with('success','Item Updated Successfully');
    }

    public function delete($id){
        $product = Product::find($id);
        $product->delete();

        return redirect()->route('admins.productlist')->with('success','Item Deleted Successfully');
    }

    public function transactionlist(){
        $transaction = Transaction::all();
        return view('admin.transaction.index')->with('products', $transaction); 
    }

    public function transactionview($id){
        $sproducts = Transaction::where('id', $id)->get();
        return view('admin.transaction.view')->with('transaction',$sproducts);
    }



}
