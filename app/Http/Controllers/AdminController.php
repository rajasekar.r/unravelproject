<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index(){
        
        return view('admin.dashboard'); 
     }
 
     public function login(){
         return view('admin.login');
     }
 
     public function handleLogin(Request $req){
	     $a= 'test1';
	     $credentials = $req->only('email', 'password');
         if(Auth::guard('webadmin')->attempt($credentials)){
            return redirect()->route('admins.dashboard');
         }else{
             echo 'Wrong Credentials';
             return redirect()->route('admins.login');
         }
     }
 
     public function logout(){
         Auth::logout();
         return redirect()->route('admins.login');
     }
}
