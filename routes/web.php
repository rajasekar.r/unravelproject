<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 // Admin
Route::get('/admins/login',[AdminController::class,'login'])->name('admins.login');
Route::post('/admins/handlelogin',[AdminController::class,'handleLogin'])->name('admins.handleLogin');
Route::get('/admins/logout',[AdminController::class,'logout'])->name('admins.logout');


Route::get('/admins/dashboard',[AdminController::class,'index'])->name('admins.dashboard');

//Users
Route::get('/admins/userlist',[UserController::class,'index'])->name('admins.userlist');
Route::get('/admins/userview/{userid}',[UserController::class,'view'])->name('admins.userview');

//Transactions
Route::get('/admins/transactionlist',[ProductController::class,'transactionlist'])->name('admins.transactionlist');
Route::get('/admins/transactionview/{trans_id}',[ProductController::class,'transactionview'])->name('admins.transactionview');

//Product
Route::get('/admins/productlist/',[ProductController::class,'index'])->name('admins.productlist');
Route::get('/admins/addproduct/',[ProductController::class,'create'])->name('admins.addproduct');
Route::post('/admins/storeproduct/',[ProductController::class,'store'])->name('admins.storeproduct');
Route::get('/admins/editproduct/{prod_id}',[ProductController::class,'edit'])->name('admins.editproduct');
Route::post('/admins/updateproduct/',[ProductController::class,'update'])->name('admins.updateproduct');
Route::get('/admins/viewproduct/{prod_id}',[ProductController::class,'view'])->name('admins.viewproduct');
Route::get('/admins/deleteproduct/{prod_id}',[ProductController::class,'delete'])->name('admins.deleteproduct');


//Site
Route::get('/',[UserController::class,'registration']);
Route::get('/site/userregistration/',[UserController::class,'registration'])->name('site.userregistration');
Route::get('/site/login/',[UserController::class,'login'])->name('site.login');
Route::get('/site/logout/',[UserController::class,'logout'])->name('site.logout');
Route::post('/site/storeuser/',[UserController::class,'storeuser'])->name('site.storeuser');
Route::post('/site/handleLogin/',[UserController::class,'handleLogin'])->name('site.handleLogin');

//Site Product
Route::get('/site/showproduct/',[CartController::class,'showproduct'])->name('site.showproduct');
Route::get('/site/showcart/',[CartController::class,'showcart'])->name('site.showcart');
Route::get('/site/checkout/',[CartController::class,'checkout'])->name('site.checkout');
Route::get('/site/transaction/',[CartController::class,'transaction'])->name('site.transaction');
Route::post('/site/storecart/',[CartController::class,'storecart'])->name('site.storecart');
Route::post('/site/storetransaction/',[CartController::class,'storetransaction'])->name('site.storetransaction');